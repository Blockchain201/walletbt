import {
    Text,
    View,
    StyleSheet,
    FlatList,
    Pressable,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useEffect } from "react";
function Transaction() {
    // Show an alert when the Login screen is loaded
    useEffect(() => {
        showLoginAlert();
    }, []);

    const showLoginAlert = () => {
        alert('This is the Transaction History page.');
    }
    return (
        <View style={styles.rootContainer}>
            <View style={styles.modalContainer}>
                <FlatList
                    ListHeaderComponent={() => (
                        <View style={styles.headerContainer}>
                            <Text style={styles.headerText}>Transaction
                                History</Text>
                            <Pressable style={styles.accountGroupContainer}>
                                <Text style={{
                                    paddingTop: 5, fontWeight: "bold"
                                }}>Select Account Address</Text>
                                <Ionicons
                                    name="ios-chevron-down-circle-sharp"
                                    size={32}
                                    color="Black"
                                    style={styles.dropDownIcon}
                                />
                            </Pressable>
                        </View>
                    )}
                    ListFooterComponent={() => (
                        <Text style={styles.emptyTransactionText}>
                            No Transactions Record History Found
                        </Text>
                    )} />
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    headerContainer: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#0a4f95",
        paddingBottom: 20,
    },
    headerText: {
        fontSize: 30,
        fontWeight: "bold",
        color: "white",
        paddingVertical: 10,
    },
    accountGroupContainer: {
        backgroundColor: "#FFFFFF",
        flexDirection: "row",
        paddingVertical: 10,
        paddingHorizontal: 30,
        borderRadius: 20,
    },
    dropDownIcon: {
        paddingLeft: 10,
        justifyContent: "center",
        alignItems: "center",
    },
    emptyTransactionText: {
        fontSize: 18,
        fontWeight: "bold",
        paddingTop: 20,
        paddingHorizontal: 5,
        textAlign: "center",
    },
});
export default Transaction;