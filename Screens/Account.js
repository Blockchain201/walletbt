import { useEffect, useState } from "react";
import {
    Text,
    View,
    ActivityIndicator,
    StyleSheet,
    Button,
    Pressable,
    Alert,
    Modal,
    FlatList,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ethers } from "ethers";
import truncateEthAddress from "truncate-eth-address";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch } from "react-redux";
import { setAddress, setPK, setWalletBT } from "../Store/walletBTSlice";
const INFURA_SEPOLIA_API = "https://sepolia.infura.io/v3/6d083c350fde4e4ba4a4c4b8253feb82";

function Account() {
    const dispatch = useDispatch();
    const [walletBTAccounts, setWalletBTAccounts] = useState();
    const [walletBTPP, setWalletBTPP] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [currentAccountAddress, setCurrentAccountAddress] = useState();
    const [currentAccountPK, setCurrentAccountPK] = useState();
    const [balance, setBalance] = useState(0.0);
    const [keyStatus, setKeyStatus] = useState(false);
    const [createAccountStatus, setCreateAccountStatus] = useState(false);
    const [modalStatus, setModalStatus] = useState(false);
    const [transactionsModalVisible, setTransactionsModalVisible] = useState(false);
    const [transactionHistoryStatus, setTransactionHistoryStatus] = useState(false)
    const [selectedAccountTransactions, setSelectedAccountTransactions] = useState([]);
    const sampleTransactions = [
        { id: "0x74F91c5c579952DE66d43f73C46b8580b72dC8D9", recipient: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", amount: 1.0, timestamp: "2023-11-01 12:00 PM" },
        { id: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", recipient: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", amount: 1.0, timestamp: "2023-11-01 12:00 PM" },
        { id: "0x2c3834Bc23142496ced0926862EAaEf7e4297246", recipient: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", amount: 1.0, timestamp: "2023-11-01 12:00 PM" },
        { id: "0x74F91c5c579952DE66d43f73C46b8580b72dC8D9", recipient: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", amount: 1.0, timestamp: "2023-11-01 12:00 PM" },
        { id: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", recipient: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", amount: 2.0, timestamp: "2023-11-02 01:00 PM" },
        { id: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", recipient: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", amount: 2.0, timestamp: "2023-11-02 01:00 PM" },
        { id: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", recipient: "0x4375a77aA95cA2fb537D66100C4ad1EAb65dD776", amount: 2.0, timestamp: "2023-11-02 01:00 PM" },
    ];
    const [selectedAccount, setSelectedAccount] = useState("");

    function createAccount() {
        setCreateAccountStatus(true);
        setTimeout(() => {
            const index = walletBTAccounts.index + 1;
            const path = `m/44'/60'/0'/0/${index}`;
            const hdNode = ethers.utils.HDNode.fromMnemonic(walletBTPP);
            const myAccount = hdNode.derivePath(path);
            // Using the spread operator to reconstruct my accounts
            const newWalletBTAccounts = {
                ...walletBTAccounts,
                index: index,
                walletBTAccounts: [
                    ...walletBTAccounts.walletBTAccounts,
                    { name: myAccount.address, key: myAccount.privateKey },
                ],
            };
            setAccountsLocal(newWalletBTAccounts);
            dispatch(setWalletBT(newWalletBTAccounts));
            setCreateAccountStatus(false);
            Alert.alert(
                "Account Creation",
                `You have successfully created an account with address ${truncateEthAddress(myAccount.address)}`,
                [
                    {
                        text: "Ok",
                    },
                ]
            );
        }, 500);
    }

    async function setAccountsLocal(walletBT) {
        try {
            const accounts = JSON.stringify(walletBT);
            await AsyncStorage.setItem("walletBTAccounts", accounts);
        } catch (e) {
            alert(e);
        }
    }
    const transactionHistory = (name) => {
        setTransactionHistoryStatus(true)
        setSelectedAccount(name)

    }
    useEffect(() => {
        const walletAddresses = async () => {
            try {
                const addresses = await AsyncStorage.getItem("walletBTAccounts");
                const passphrase = await AsyncStorage.getItem("walletBTPP");
                const formattedAddresses = JSON.parse(addresses);
                setWalletBTAccounts(formattedAddresses);
                dispatch(setWalletBT(formattedAddresses));
                setWalletBTPP(passphrase);
                setIsLoading(false);
            } catch (error) {
                alert(error);
            }
        };
        walletAddresses();
        if (currentAccountPK) {
            try {
                const provider = new ethers.providers.JsonRpcProvider(INFURA_SEPOLIA_API);
                const wallet = new ethers.Wallet(currentAccountPK, provider);
                // Async call to retrieve balance
                wallet
                    .getBalance()
                    .then((balance) => {
                        setBalance(ethers.utils.formatEther(balance));
                    })
                    .catch((e) => {
                        alert(e);
                    });
            } catch (error) {
                alert(error);
            }
        }
    }, [currentAccountAddress, balance]);

    useEffect(() => {
        if (!isLoading) {
            setCurrentAccountAddress(walletBTAccounts.walletBTAccounts[0].name);
            dispatch(setAddress(walletBTAccounts.walletBTAccounts[0].name));
            setCurrentAccountPK(walletBTAccounts.walletBTAccounts[0].key);
            dispatch(setPK(walletBTAccounts.walletBTAccounts[0].key));
        }
    }, [isLoading]);

    useEffect(() => {
        const walletBTAccounts = async () => {
            try {
                const addresses = await AsyncStorage.getItem("walletBTAccounts");
                const formattedAddresses = JSON.parse(addresses);
                setWalletBTAccounts(formattedAddresses);
                dispatch(setWalletBT(formattedAddresses));
            } catch (error) {
                alert(error);
            }
        };
        walletBTAccounts();
    }, [createAccountStatus]);

    // Show an alert when the Login screen is loaded
    useEffect(() => {
        showLoginAlert();
    }, []);

    const showLoginAlert = () => {
        alert('This is the Account Details page.');
    };

    return (
        <View style={styles.rootContainer}>
            {isLoading ? (
                <ActivityIndicator size="large" color="white" />
            ) : (
                <View style={styles.bodyContainer}>
                    <Modal
                        animationType="slide"
                        onRequestClose={() => setModalStatus(false)}
                        visible={modalStatus}
                    >
                        <View style={styles.modalContainer}>
                            <FlatList
                                ListHeaderComponent={() => (
                                    <Text style={styles.modalHeader}>List of Your Accounts</Text>
                                )}
                                ListFooterComponent={() => (
                                    <Text style={styles.modalFooter}>Click on an account to select it</Text>
                                )}
                                data={walletBTAccounts.walletBTAccounts}
                                renderItem={({ item }) => (
                                    <View style={styles.accountItemContainer}>
                                        <View style={styles.accountItem}>
                                            <View style={styles.accountItemText}>
                                                <Text style={styles.modalAccountNameLabel}>{item.name}</Text>
                                                {console.log("a,sd", item.name)}
                                                {console.log("selected account", selectedAccount)}

                                            </View>
                                            <View style={styles.accountItemButton}>
                                                <Button
                                                    title="View 
                                                    Transactions"
                                                    onPress={() => transactionHistory(item.name)}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                )}
                                keyExtractor={(item) => item.name}
                            />
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        onRequestClose={() => setTransactionHistoryStatus(false)}
                        visible={transactionHistoryStatus}
                    >
                        <View style={styles.modalContainers}>
                            <View style={styles.tableHeader}>
                                {/* <Text style={styles.headerText}>ID</Text> */}
                                <Text style={styles.headerText}>Recipient</Text>
                                <Text style={styles.headerText}>Amount</Text>
                                <Text style={styles.headerText}>Timestamp</Text>
                            </View>
                            {sampleTransactions.map((details, index) => {
                                if (details.id === selectedAccount) {
                                    return (
                                        <View key={index} style={styles.tableRow} >
                                            {/* <Text style={styles.cell}>{details.id}</Text> */}
                                            <Text style={styles.cell}>{details.recipient}</Text>
                                            <Text style={styles.cell}>{details.amount}</Text>
                                            <Text style={styles.cell}>{details.timestamp}</Text>
                                        </View>
                                    );
                                } else {
                                    return null;
                                }
                            })}
                        </View>
                    </Modal>

                    <View style={styles.headerContainer}>
                        <Text style={styles.headerTitle}>Sepolia Test Network</Text>
                        <Text style={styles.headerTagLine}>
                            Currently you are using the Ethereum test network
                        </Text>
                    </View>
                    <View style={styles.body}>
                        <View>
                            <Text style={styles.label}>Current Account Address:</Text>
                            <Text style={styles.value} selectable={true}>
                                {currentAccountAddress}
                            </Text>
                        </View>
                        <View style={styles.prvateKeyContainer}>
                            <Text style={styles.label}>
                                Click the button below to get your private key
                            </Text>
                            {!keyStatus ? (
                                <Pressable
                                    style={styles.getPrivateKey}
                                    onPress={() => setKeyStatus(true)}
                                >
                                    <Text style={styles.getPrivateKeyButton}>Get Private Key</Text>
                                </Pressable>
                            ) : (
                                <>
                                    <Text>{currentAccountPK}</Text>
                                    <Button title="Hide" onPress={() => setKeyStatus(false)} />
                                </>
                            )}
                        </View>
                        <View style={styles.prvateKeyContainer}>
                            <Text style={styles.label}>Create New Account</Text>
                            {!createAccountStatus ? (
                                <Ionicons
                                    style={{ paddingTop: 10 }}
                                    name="add-circle-sharp"
                                    size={42}
                                    color="green"
                                    onPress={() => createAccount()}
                                />
                            ) : (
                                <ActivityIndicator size="large" color="white" />
                            )}
                        </View>
                        <View style={styles.prvateKeyContainer}>
                            <Text style={styles.label}>Change the Account</Text>
                            <Pressable
                                onPress={() => setModalStatus(true)}
                                style={styles.changeAccountButton}
                            >
                                <Text style={{ paddingTop: 5 }}>
                                    {currentAccountAddress ? truncateEthAddress(currentAccountAddress) : null}
                                </Text>
                                <Ionicons
                                    name="ios-chevron-down-circle-sharp"
                                    size={32}
                                    color="white"
                                />
                            </Pressable>
                        </View>
                        <View style={styles.footer}>
                            {balance ? (
                                <>
                                    <Text style={styles.balanceLabel}>Balance</Text>
                                    <Text style={styles.balance}>
                                        {parseFloat(balance).toFixed(5)}
                                    </Text>
                                </>
                            ) : (
                                <>
                                    <ActivityIndicator size="large" color="white" />
                                    <Text style={styles.footerBalanceLoadingText}>
                                        Loading the balance
                                    </Text>
                                </>
                            )}
                        </View>
                    </View>
                </View>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    headerContainer: {
        alignItems: "center",
        backgroundColor: "#0a4f95",
        paddingVertical: 20,
    },
    headerTitle: {
        fontSize: 30,
        fontWeight: "bold",
        color: "white",
        paddingVertical: 20,
    },
    headerTagLine: {
        color: "white",
        fontSize: 17,
    },
    body: {
        flex: 1,
        justifyContent: "space-between",
    },
    label: {
        fontSize: 20,
        fontWeight: "bold",
        paddingVertical: 10,
        paddingLeft: 20,
    },
    value: {
        fontSize: 15,
        fontWeight: "bold",
        paddingVertical: 5,
        paddingLeft: 20,
    },
    bodyContainer: {
        flex: 1,
        justifyContent: "space-between",
    },
    footer: {
        backgroundColor: "#31689f",
        paddingVertical: 25,
        justifyContent: "center",
        alignItems: "center",
    },
    footerBalanceLoadingText: {
        color: "white",
        fontSize: 16,
    },
    prvateKeyContainer: {
        alignItems: "center",
        paddingLeft: 5,
        paddingRight: 10,
    },
    balance: {
        color: "white",
        fontSize: 24,
        textAlign: "center",
        fontWeight: "bold",
    },
    balanceLabel: {
        fontSize: 16,
        color: "white",
    },
    getPrivateKey: {
        borderRadius: 20,
        backgroundColor: "green",
        paddingVertical: 10,
        paddingHorizontal: 20,
        alignSelf: "center",
    },
    getPrivateKeyButton: {
        color: "white",
    },
    modalContainer: {
        flex: 1,
        backgroundColor: "#a4c5e6",
        justifyContent: "center",
        alignItems: "center",
    },
    changeAccountButton: {
        backgroundColor: "green",
        flexDirection: "row",
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    modalAccountNameLabel: {
        width: 200,
        backgroundColor: "#0a4f95",
        color: "white",
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderBottomRightRadius: 20,
    },
    modalHeader: {
        fontSize: 18,
        fontWeight: "bold",
        paddingVertical: 20,
        alignSelf: "center",
    },
    modalFooter: {
        color: "red",
        marginTop: 10,
        fontSize: 16,
        backgroundColor: "#dfe8f0",
        paddingVertical: 20,
        textAlign: "center",
    },
    accountItemContainer: {
        width: 350,
        backgroundColor: "#ffffff",
        borderRadius: 10,
        marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    accountItem: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 15,
    },
    accountItemText: {
        flex: 1,
    },
    accountItemButton: {
        width: 100,

    },
    viewTransactionsButton: {
        backgroundColor: "blue",
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 8,
        justifyContent: "center",
        alignItems: "center",
    },
    viewTransactionsButtonText: {
        color: "white",
        fontWeight: "bold",
    },



    modalContainers: {
        flex: 1,
        backgroundColor: 'white',
        padding: 20,
    },
    tableHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        marginBottom: 10,
        paddingBottom: 5,
    },
    headerText: {
        fontWeight: 'bold',

    },
    tableRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    cell: {
        flex: 1,
    },
});

export default Account;